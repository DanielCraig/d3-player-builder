import { createMuiTheme } from '@material-ui/core';

export const theme = createMuiTheme({
  palette: {
    text: {
      primary: 'rgba(232, 182, 92)'
    }
  },
  typography: {
    body1: {
      color: 'rgba(232, 182, 92)'
    }
  }
})