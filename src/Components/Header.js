import React, { useState, useEffect } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import D3Icon from '../assets/png/d3Logo.png';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  appBarStyle: {
    backgroundColor: '#262D37',
  },
  menuButton: {
    marginRight: theme.spacing(2),
    color: '#FF6666'
  },
  profileIcon: {
    margin: 0,
    padding: 0,
    paddingLeft: '15px',
  },
  title: {
    flexGrow: 1,
    display: 'none',
    color: '#FF6666',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  loginButton: {
    paddingLeft: 10
  },
}));

export const Header = ({ user }) => {
  const classes = useStyles();
  const [auth, setAuth] = useState(false);

  useEffect(() => {
    console.log('user', user);
    if (user !== null) {
      setAuth(true);
    }
  }, [user])

  const handleMenu = event => {
    console.log('login button pressed')
  };

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appBarStyle}>
        <Toolbar variant="dense">
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
          >
            <MenuIcon />
          </IconButton>
          <img alt='logo' width="35" height="35" src={D3Icon} />
          <Typography className={classes.title} variant="h6" noWrap>
            D3 Player Builder
          </Typography>
          {auth && (
            <div>
              <IconButton
                className={classes.profileIcon}
                onClick={handleMenu}
                color='inherit'
              >
                <Typography className={classes.loginButton}> Login </Typography>
              </IconButton>
            </div>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default Header;