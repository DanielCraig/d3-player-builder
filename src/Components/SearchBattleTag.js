import React, { useState } from 'react';
import { FormControl, Select, MenuItem, makeStyles, fade, Typography, TextField, Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { getUser } from '../Redux/User/Actions';

const useStyles = makeStyles(theme => ({
  inputBase: {
    textAlign: 'center',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    color: 'darkpurple',
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 200,
      '&:focus': {
        width: 220,
      },
    },
  },
}))


const SearchBattleTag = ({ getUser, accessToken }) => {
  const [battletag, setBattletag] = useState('');

  const classes = useStyles();

  const handleBattletagField = event => {

    const { name, value } = event.target;

    if (name === 'battletag') {
      setBattletag(value);
    }
  }

  const handleBattletag = () => {
    getUser(battletag, 'eu', accessToken)
  }


  return (
    <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', padding: '20px 0px', height: 'auto' }}>
      <div style={{ display: 'flex', flexDirection: 'row' }}>
        <Typography style={{ paddingRight: 50, paddingTop: 10 }}> Choose your region: </Typography>
        <FormControl className={classes.formControl}>
          <Select
            className={classes.inputBase}
            autoWidth
            labelId="demo-simple-select-label"
            id="demo-simple-select"
          >
            <MenuItem value='us'>US</MenuItem>
            <MenuItem value='eu'>EU</MenuItem>
          </Select>
        </FormControl>
      </div>
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ paddingTop: 20, paddingRight: 20, color: '#E8B65C' }}> Battletag:  </Typography>
        <TextField onChange={handleBattletagField} name="battletag" label="Battletag" />
        <Button onClick={handleBattletag} style={{ marginTop: 40, paddingLeft: 20, marginBottom: 20, color: '#E8B65C' }}>
          Search
        </Button>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  playerHeroes: state.userHeroes,
  accessToken: state.accessToken
})

const mapDispatchToProps = {
  getUser,
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBattleTag);