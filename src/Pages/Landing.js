import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Header from './../Components/Header';
import SearchBattleTag from './../Components/SearchBattleTag';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Container, Paper, TextField, Button, CircularProgress, IconButton } from '@material-ui/core';
import { getUser, getAccessToken } from './../Redux/User/Actions';
import { connect } from 'react-redux';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ClearIcon from '@material-ui/icons/Clear';
import { getFemaleCharacterImage, getMaleCharacterImage } from '../Utils/common.utils';

const useStyles = makeStyles(theme => ({
  mainContainer: {
    padding: '20px',
    backgroundColor: '#212121',
    height: '100vh',
  },
}))


const Landing = ({ playerHeroes, accessToken, getAccessToken }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [canSubmit, setCanSubmit] = useState(true);
  const [isAuthenticating, setIsAuthenticating] = useState(false);
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const classes = useStyles();

  useEffect(() => {
    if(!accessToken) {
      getAccessToken();
    }
  }, []);

  useEffect(() => {
    if (username.length > 4 && password.length > 4) {
      setCanSubmit(false);
    } else {
      setCanSubmit(true)
    }
  }, [username, password])

  const handleFields = event => {

    const { name, value } = event.target;

    if (name === 'username') {
      setUsername(value);
    }

    if (name === 'password') {
      setPassword(value);
    }
  }

  const authorizeUser = () => {
    if (username.length > 4 && password.length > 4) {
      setIsAuthenticating(true);
      setIsAuthenticated(true);
    }
  }

  return (
    <div>
      <Header />
      <div className={classes.mainContainer}>
        <Container>
          <Paper style={{ backgroundColor: '#262D37', display: 'flex', flexDirection: 'column', justifyContent: 'center', marginTop: 50 }}>
            {!isAuthenticated ? (
              <>
                <div style={{ justifyContent: 'center', display: 'flex', marginTop: 20 }}>
                  <Typography variant="h6" style={{ color: '#FF6666' }}> Login </Typography>
                </div>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                  <form className={classes.root}>
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                      <TextField onChange={handleFields} name="username" label="Username" />
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                      <TextField type='password' onChange={handleFields} name="password" label="Password" />
                      <Button disabled={canSubmit} onClick={authorizeUser} style={{ marginTop: 20, marginBottom: 20 }}>
                        {isAuthenticating ? <CircularProgress size={24} thickness={3} /> : 'Go!'}
                      </Button>
                    </div>
                  </form>
                </div>
              </>
            ) : (
                <div style={{ height: 'auto', justifyContent: 'space-around', alignItems: 'center', display: 'flex', flexDirection: 'column' }}>
                  <Typography variant="h6" style={{ color: 'FF6666' }}> Search for your battletag here </Typography>
                  <SearchBattleTag />
                  <div style={{ display: 'flex', flexDirection: 'column', padding: 20 }}>
                    {playerHeroes.length > 1 ?
                      (
                        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignContent: 'center' }}>
                          <Typography style={{ fontWeight: 'bold', marginBottom: 10, color: 'FF6666', textAlign: 'center' }}> Are these heroes yours? </Typography>
                          <div style={{ paddingLeft: 10 }}>
                            <IconButton size="small">
                              <CheckCircleIcon fontSize="small" style={{ color: 'green' }} />
                            </IconButton>
                            <IconButton size="small">
                              <ClearIcon fontSize="small" style={{ color: 'red' }} />
                            </IconButton>
                          </div>
                        </div>
                      )
                      :
                      null
                    }
                    {playerHeroes.slice(0, 4).map(hero => (
                      <div style={{ flexDirection: 'row', display: 'flex', }}>
                        {hero.gender === 1 ?
                          <img alt="char" src={getFemaleCharacterImage(hero.classSlug)} width="24" height="24" style={{ padding: 4 }} />
                          :
                          <img alt="char" src={getMaleCharacterImage(hero.classSlug)} width="24" height="24" style={{ padding: 4 }} />
                        }
                        <Typography style={{ padding: 4 }} key={hero.id} > {hero.name} ({hero.class}, {hero.level}) at paragon {hero.paragonLevel} </Typography>
                      </div>
                    ))}
                  </div>
                </div>
              )}
          </Paper>
        </Container>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  playerHeroes: state.userHeroes,
  accessToken: state.accessToken
})

const mapDispatchToProps = {
  getUser,
  getAccessToken,
}


export default connect(mapStateToProps, mapDispatchToProps)(Landing);
