import DemonHunterFemale from '../assets/png/characters/dh-female.png';
import DemonHunterMale from '../assets/png/characters/dh-male.png';
import MonkMale from '../assets/png/characters/monk-male.png';
import MonkFemale from '../assets/png/characters/monk-female.png';
import ShamanFemale from '../assets/png/characters/shaman-female.png';
import ShamanMale from '../assets/png/characters/shaman-male.png';
import NecromancerFemale from '../assets/png/characters/necromancer-female.png';
import NecromancerMale from '../assets/png/characters/necromancer-male.png';
import BarbarianFemale from '../assets/png/characters/barbarian-female.png'
import BarbarianMale from '../assets/png/characters/barbarian-male.png'

export const getFemaleCharacterImage = (slug) => {
  switch (slug) {
    case 'monk':
      return MonkFemale;
    case 'demon-hunter':
      return DemonHunterFemale;
    case 'necromancer':
      return NecromancerFemale;
    case 'crusader':
      return ''
    case 'barbarian':
      return BarbarianFemale;
    case 'mage':
      return ''
    case 'shaman':
      return ShamanFemale;
    default: return null;
  }
}
 
export const getMaleCharacterImage = (slug) => {
  switch (slug) {
    case 'monk':
      return MonkMale;
    case 'demon-hunter':
      return DemonHunterMale;
    case 'necromancer':
      return NecromancerMale;
    case 'crusader':
      return ''
    case 'barbarian':
      return BarbarianMale;
    case 'mage':
      return ''
    case 'shaman':
      return ShamanMale;
    default: return null;
  }
}