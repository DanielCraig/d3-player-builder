import {
  FETCH_USER,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAIL,
  CLEAR_STORE,
  GET_ACCESS_TOKEN,
  GET_ACCESS_TOKEN_SUCCESS
}
  from './Types';

const initialState = {
  loading: false,
  battleTag: '',
  guildName: '',
  highestHardcoreLevel: null,
  userHeroes: [],
  kills: {},
  lastHeroPlayed: '',
  lastUpdated: '',
  maxParagonLevel: null,
  maxParagonLevelHardcore: null,
  maxParagonLevelSeasonal: null,
  maxParagonLevelSeasonalHardcore: null,
  error: '',
  accessToken: null,
  expiry: null
}

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER:
      return {
        ...state,
        loading: true
      }

    case FETCH_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        battleTag: action.payload.data.battleTag,
        guildName: action.payload.data.guildName,
        highestHardcoreLevel: action.payload.data.highestHardcoreLevel,
        userHeroes: action.payload.data.heroes,
        kills: action.payload.data.kills,
        lastHeroPlayed: action.payload.data.lastHeroPlayed,
        lastUpdated: action.payload.data.lastUpdated,
        maxParagonLevel: action.payload.data.paragonLevel,
        maxParagonLevelHardcore: action.payload.data.paragonLevelHardcore,
        maxParagonLevelSeasonal: action.payload.data.paragonLevelSeason,
        maxParagonLevelSeasonalHardcore: action.payload.data.paragonLevelSeasonHardcore,
        error: ''
      }

    case FETCH_USER_FAIL:
      return {
        ...state,
        error: action.error.response.data.reason || action.error.message,
        loading: false
      }

    case CLEAR_STORE:
      return initialState;

    case GET_ACCESS_TOKEN:
      return {
        ...state,
        loading: true
      }

    case GET_ACCESS_TOKEN_SUCCESS:
      return {
        ...state,
        loading: false,
        accessToken: action.payload.data.access_token,
        expiry: action.payload.data.expires_in
      }

    default:
      return state;
  }
}