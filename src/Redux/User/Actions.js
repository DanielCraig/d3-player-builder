import { FETCH_USER, CLEAR_STORE, GET_ACCESS_TOKEN } from './Types';

export const getAccessToken = () => {
  return {
    type: GET_ACCESS_TOKEN,
    payload: {
      request: {
        method: 'post',
        baseURL: 'https://eu.battle.net/',
        url: 'oauth/token',
        auth: {
          username: 'a28eabd572e0407cba72a79887fa3afa',
          password: 'xK67QVYkdcJcTJbcYJzodx1XF2mg0tjs'
        },
        data: 'grant_type=client_credentials',
      }
    }
  }
};

export const getUser = (battletag, region, access_token) => {
  return {
    type: FETCH_USER,
    payload: {
      request: {
        method: 'get',
        url: `https://${region}.api.blizzard.com/d3/profile/${battletag}/?locale=en_US&access_token=${access_token}`,
      },
    }
  };
}

export const clearStore = () => {
  return {
    type: CLEAR_STORE
  }
}