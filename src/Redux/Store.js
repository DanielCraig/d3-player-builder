import { createStore, applyMiddleware } from 'redux';
import axiosMiddleware from 'redux-axios-middleware';
import axios from 'axios';
import logger from 'redux-logger'
import { userReducer } from './User/Reducer';
import { configureStore } from '@reduxjs/toolkit'

export const store = configureStore({
  reducer: userReducer,
  middleware: [axiosMiddleware(axios), logger],
})
