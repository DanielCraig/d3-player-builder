import React from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import Landing from './Pages/Landing';
import { store } from './Redux/Store';
import 'typeface-roboto'
import { ThemeProvider } from '@material-ui/core';
import { theme } from './main-theme';

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route path='/' exact component={Landing} />
          </Switch>
        </Router>
      </ThemeProvider>
    </Provider>
  );
}

export default App;
